#!/bin/bash

if [[ -z "$NOROOT" ]]; then
  NOROOT=true
fi

if [[ $# -eq 1 ]]; then
  TARGET="$1"
fi

if [[ -z "$TARGET" ]]; then
  TARGET="$(hostname)"
fi

function _stow() {
  # if a host-specific entry exists, use it isntead of the generic one
  hostver="$1-$TARGET"
  if [[ -d "$hostver" ]]; then
    echo "   Stowing $hostver..."
    stow -t "$HOME/" "$hostver" --ignore='^(?!\.).+$' ||
      echo "ERROR: stowing failed...Exiting."
    exit 1
  else
    if [[ -d "$1" ]]; then
      echo "   Stowing $1..."
      stow -t "$HOME/" "$1" --ignore='^(?!\.).+$' ||
        (
          echo "ERROR: stowing failed...Exiting."
          exit 1
        )
    else
      echo "   $1 not found...ignoring..."
    fi
  fi

  if [[ $? -eq 1 ]]; then
    echo "Error: Something went wrong.."
    exit 1
  fi
}

_stow zsh
echo "   creating $HOME/.cache/zsh"
mkdir -p "$HOME/.cache/zsh"

echo -e "\n##BASH\n"
_stow bash

echo -e "\n##PROFILE.D\n"
_stow profile.d

echo -e "\n## PROFILE"
_stow profile

echo -e "\n## NEOVIM\n"
_stow nvim

echo -e "\n## WEECHAT\n"
_stow weechat

echo -e "\n## MPV\n"
_stow mpv

echo -e "\n## DUNST\n"
_stow dunst

echo -e "\n## COMPTON\n"
_stow compton

echo -e "\n## NCMPCPP\n"
_stow ncmpcpp

echo -e "\n## QN\n"
_stow qn

echo -e "\n## mbrun\n"
_stow mbrun

echo -e "\n## RANGER\n"
_stow ranger

echo -e "\n## TMUX\n"
_stow tmux

echo -e "\n## I3STATUS\n"
_stow i3status

echo -e "\n## SWAY\n"
_stow sway

echo -e "\n## I3\n"
_stow i3

echo -e "\n## BSPWM\n"
_stow bspwm

echo -e "\n## POLYBAR\n"
_stow polybar

echo -e "\n## AUTORANDR\n"
_stow autorandr

echo -e "\n## SXHKD\n"
_stow sxhkd

echo -e "\n## I3BLOCKS\n"
_stow i3blocks

echo -e "\n## MPD\n"
_stow mpd

echo -e "\n## XINITRC\n"
_stow xinitrc

echo -e "\n## XSETTINGS\n"
_stow xsettings

echo -e "\n## STARDICT\n"
_stow stardict

echo -e "\n## SLOP\n"
_stow slop

echo -e "\n## ROFI\n"
_stow rofi

echo -e "\n##QUTEBROWSER\n"
_stow qutebrowser

echo -e "\n##EVENTD\n"
_stow eventd

echo -e "\n##NEWSBOAT\n"
mkdir -p "$XDG_DATA_HOME/newsboat"
_stow newsboat

echo -e "\n##BASHMOUNT\n"
_stow bashmount

echo -e "\n##ALACRITTY\n"
_stow alacritty

echo -e "\n##EMACS\n"
_stow emacs

echo -e "\n##ZK\n"
_stow zk
