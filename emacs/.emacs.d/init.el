;; -*- lexical-binding: t; -*-

;; Adapted from: https://github.com/daviwil/dotfiles/blob/master/.emacs.d

(defvar mbf/is-termux
  (string-suffix-p "Android" (string-trim (shell-command-to-string "uname -a"))))

(defvar mbf/current-distro (or (and (eq system-type 'gnu/linux)
                                   (file-exists-p "/etc/os-release")
                                   (with-temp-buffer
                                     (insert-file-contents "/etc/os-release")
                                     (search-forward-regexp "^ID=\"?\\(.*\\)\"?$")
                                     (intern (or (match-string 1)
                                                 "unknown"))))
                              'unknown))

;; Add configuration modules to load path
(add-to-list 'load-path '"~/.emacs.d/modules")

(require 'mbf-package)
(require 'mbf-settings)
(require 'mbf-keys)

(require 'mbf-core)
(require 'mbf-interface)
(require 'mbf-dev)
(require 'mbf-workflow)
