;; -*- lexical-binding: t; -*-

;; -- Key Binding Helpers -----

(defun define-key* (keymap &rest keys)
  "Define multiple keys in a keymap."
  (while keys
    (define-key keymap
                (kbd (pop keys))
                (pop keys))))

(put 'define-key* 'lisp-indent-function 1)

;; -- Files Keymap -----

(defvar mbf/files-prefix-map (make-sparse-keymap)
  "Keymap for common file operations.")

(global-set-key (kbd "C-c f") mbf/files-prefix-map)

;; -- Git Keymap -----

(defvar mbf/git-prefix-map (make-sparse-keymap)
  "Keymap for Git operations.")

(global-set-key (kbd "C-c g") mbf/git-prefix-map)

(require 'mbf-keys-evil)

(provide 'mbf-keys)
