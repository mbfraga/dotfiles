;; -*- lexical-binding: t; -*-

(defun mbf/load-system-settings ()
  (interactive)
  (load-file "~/.emacs.d/per-system-settings.el"))

(defun mbf/system-settings-get (setting)
  (alist-get setting mbf/system-settings))

;; Load settings for the first time
(mbf/load-system-settings)


(provide 'mbf-settings)
