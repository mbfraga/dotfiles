if [[ -e "$HOME/.profile" ]]; then
   source "$HOME/.profile"
fi


if [[ -e "$HOME/.bashrc" ]]; then
   source "$HOME/.bashrc"
fi

if [[ -e "$HOME/.local/share/cargo/env" ]]; then
   . "$HOME/.local/share/cargo/env"
fi
