# ~/.bashrc


if [[ $- != *i*  ]]; then
   # Shell is non-interactive. Be done now!
   return
fi


# Source $XDG_CONFIG_HOME/profile/*
# I keep my aliases and generic functions in there.
if [[ -d "$XDG_CONFIG_HOME/profile.d" ]]; then
   for f in $XDG_CONFIG_HOME/profile.d/*; do
      . "$f"
   done
fi

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize

# Disable completion when the input buffer is empty.  i.e. Hitting tab
# and waiting a long time for bash to expand all of $PATH.
shopt -s no_empty_cmd_completion

# Enable history appending instead of overwriting when exiting.  #139609
shopt -s histappend

# FZF
if [[ -f /usr/share/fzf/shell/key-bindings.bash ]]; then
  . /usr/share/fzf/shell/key-bindings.bash
else
  echo '/usr/share/fzf/shell/key-bindings.bash not found.'
  echo 'Install and (if necessary), source fzf'
fi


if [[ -f ~/.fzf.bash ]]; then
  source ~/.fzf.bash
fi

if command -v rg >/dev/null 2>&1; then
  # Setting fd as the default source for fzf
  export FZF_DEFAULT_COMMAND="rg --files --hidden --smart-case"
fi

export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND --glob '!*.{o,java,off,png,mps,obj}'"


if [[ -e "$HOME/.local/share/cargo/env" ]]; then
   . "$HOME/.local/share/cargo/env"
fi
