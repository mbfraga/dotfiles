# Source $HOME/.profile/*
if [[ -f "$HOME/.profile" ]]; then
   . "$HOME/.profile" ]]
fi

# Source $XDG_CONFIG_HOME/profile/*
# I keep my aliases and generic functions in there.
if [[ -d "$XDG_CONFIG_HOME/profile.d" ]]; then
   for f in $XDG_CONFIG_HOME/profile.d/*; do
      . "$f"
   done
fi

if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx; vlock
  #exec sway; vlock
fi

export PATH="/home/martin/.local/share/cargo/bin:$PATH"
