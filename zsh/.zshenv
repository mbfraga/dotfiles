

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

#export ZDOTDIR="$XDG_CONFIG_HOME/zsh"


export CARGO_HOME="$XDG_DATA_HOME"/cargo
export CCACHE_DIR="$XDG_CACHE_HOME"/ccache
export GIMP2_DIRECTORY="$XDG_CONFIG_HOME"/gimp
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history

if [[ -e "$HOME/.local/share/cargo/env" ]]; then
   . "$HOME/.local/share/cargo/env"
fi
