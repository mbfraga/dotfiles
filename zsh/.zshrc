#!/bin/zsh
#-----------------#
# ZSH source file #
#-----------------#
# https://github.com/jleclanche/dotfiles/blob/master/.zshrc
# https://github.com/MrElendig/dotfiles-alice/blob/master/.zshrc
# https://github.com/slashbeast/things/blob/master/configs/DOTzshrc

# eval $(keychain --eval --quiet --agents ssh id_rsa)

#--[ Enable colors ]--
autoload -U colors && colors


#--[ Load modules ]--
autoload -Uz run-help zmv

#--[ VI mode] ]--

source ~/.config/zsh/vi-mode.zsh/vi-mode.zsh

#--[ FZF ]--

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

#--[ Enable vcs ]--

autoload -Uz vcs_info

#--[ Prompt ]--

source ~/.config/zsh/git-prompt.zsh/git-prompt.zsh

ZSH_GIT_PROMPT_FORCE_BLANK=1
ZSH_GIT_PROMPT_ENABLE_SECONDARY=0
ZSH_GIT_PROMPT_SHOW_UPSTREAM="no"

ZSH_THEME_GIT_PROMPT_PREFIX=""
ZSH_THEME_GIT_PROMPT_SUFFIX=" "
ZSH_THEME_GIT_PROMPT_SEPARATOR=" "
ZSH_THEME_GIT_PROMPT_BRANCH="%{$fg_bold[cyan]%} "
ZSH_THEME_GIT_PROMPT_UPSTREAM_SYMBOL="%{$fg_bold[green]%} "
ZSH_THEME_GIT_PROMPT_UPSTREAM_NO_TRACKING="%{$fg_bold[red]%}!"
ZSH_THEME_GIT_PROMPT_UPSTREAM_PREFIX="%{$fg[red]%}(%{$fg[yellow]%}"
ZSH_THEME_GIT_PROMPT_UPSTREAM_SUFFIX="%{$fg[red]%})"
ZSH_THEME_GIT_PROMPT_DETACHED="@%{$fg_no_bold[cyan]%}"
ZSH_THEME_GIT_PROMPT_BEHIND="%{$fg_no_bold[red]%}↓"
ZSH_THEME_GIT_PROMPT_AHEAD="%{$fg_no_bold[green]%}↑"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[red]%}✖"
ZSH_THEME_GIT_PROMPT_STAGED="%{$fg[green]%}●"
ZSH_THEME_GIT_PROMPT_UNSTAGED="%{$fg[red]%}✚"
ZSH_THEME_GIT_PROMPT_UNTRACKED="…"
ZSH_THEME_GIT_PROMPT_STASHED="%{$fg[blue]%}⚑"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg_bold[green]%} "

_DECO_COLOR="%(?.%F{3}.%F{9})"

RPROMPT=''
PROMPT=$'$_DECO_COLOR┏ %f'
[ -n "$SSH_CLIENT" ] \
    && [ -n "$SSH_TTY" ] \
    && PROMPT+='%B%F{blue}@%m%f%b '   # Hostname, if in SSH session
#PROMPT+='%F{7}%B%30<..<%~%b%<<%f '    # Path truncated to 30 characters
PROMPT+='%F{7}%B%~%b%<<%f '    # Path
PROMPT+='%(12V. · %F{244} %12v%f.) ' # Python virtualenv name
PROMPT+='$(gitprompt)'                # Git status
PROMPT+=$'\n$_DECO_COLOR┗ %f'          # Newline

_WPROMPT_END="%F{3}→%f "
#_WPROMPT_END='%(?.%(!.%F{white}❯%F{yellow}❯%F{red}.%F{blue}❯%F{cyan}❯%F{green})❯%f.%F{red}❯❯❯%f) '
# Vi mode indicator, if github.com/woefe/vi-mode.zsh is loaded
if (( $+functions[vi_mode_status] )); then
    VI_INSERT_MODE_INDICATOR=$_WPROMPT_END
    VI_NORMAL_MODE_INDICATOR="[INS]$_WPROMPT_END"

    PROMPT+='$(vi_mode_status)'
else
    PROMPT+=$_WPROMPT_END
fi


setup() {
    [[ -n $_PROMPT_INITIALIZED ]] && return
    _PROMPT_INITIALIZED=1

    # Prevent Python virtualenv from modifying the prompt
    export VIRTUAL_ENV_DISABLE_PROMPT=1

    # Set $psvar[12] to the current Python virtualenv
    function _prompt_update_venv() {
        psvar[12]=
        if [[ -n $VIRTUAL_ENV ]] && [[ -n $VIRTUAL_ENV_DISABLE_PROMPT ]]; then
            psvar[12]="${VIRTUAL_ENV:t}"
        fi
    }
    add-zsh-hook precmd _prompt_update_venv

    # Draw a newline between every prompt
    function _prompt_newline(){
        if [[ -z "$_PROMPT_NEWLINE" ]]; then
            _PROMPT_NEWLINE=1
        elif [[ -n "$_PROMPT_NEWLINE" ]]; then
            echo
        fi
    }
    add-zsh-hook precmd _prompt_newline

    # To avoid glitching with fzf's alt+c binding we override the fzf-redraw-prompt widget.
    # The widget by default reruns all precmd hooks, which prints the newline again.
    # We therefore run all precmd hooks except _prompt_newline.
    function fzf-redraw-prompt() {
        local precmd
        for precmd in ${precmd_functions:#_prompt_newline}; do
            $precmd
        done
        zle reset-prompt
    }
}
setup


#--[Shell Options ]--
# name of directory will cd to it
setopt auto_cd
setopt appendhistory
setopt hist_verify
setopt hist_fcntl_lock
setopt hist_save_no_dups
setopt histignorealldups
setopt promptsubst

# disable ctrl-d closing the terminal
setopt IGNORE_EOF

# History
HISTSIZE=10000
SAVEHIST=10000
HISTFILE="$XDG_CACHE_HOME/shell_history"

mkdir -p "$HISTFILE:h"

bindkey -M isearch . self-insert # history search fix

#--[ Export variables that should have already been exported...Shame on me ]--
export LC_ALL="en_US.UTF-8"
if [[ -z "$XDG_DATA_HOME" ]]; then
   export XDG_DATA_HOME="$HOME/.local/share"
fi
if [[ -z "$XDG_CONFIG_HOME" ]]; then
   export XDG_CONFIG_HOME="$HOME/.config"
fi
if [[ -z "$XDG_CACHE_HOME" ]]; then
   export XDG_CACHE_HOME="$HOME/.cache"
fi
if [[ -z "$XDG_DATA_DIRS" ]]; then
   export XDG_DATA_DIRS="/usr/local/share:/usr/share"
fi
if [[ -z "$XDG_CONFIG_DIRS" ]]; then
   export XDG_CONFIG_DIRS="/etc/xdg"
else
   export XDG_CONFIG_DIRS="/etc/xdg:$XDG_CONFIG_DIRS"
fi
if ! [[ "${PATH}" =~ "^${HOME}/bin" ]]; then
    export PATH="${HOME}/bin:${PATH}"
fi

# Not all servers have terminfo for rxvt-256color. :<
if [ "${TERM}" = 'rxvt-256color' ] && ! [ -f '/usr/share/terminfo/r/rxvt-256color' ] && ! [ -f '/lib/terminfo/r/rxvt-256color' ] && ! [ -f "${HOME}/.terminfo/r/rxvt-256color" ]; then
    export TERM='rxvt-unicode'
fi


#--[ z ]--
export _Z_DATA="$XDG_CACHE_HOME/z"

##--[ Source other important files ]--
## I keep my aliases and generic functions in there.
if [[ -d "$XDG_CONFIG_HOME/profile.d" ]]; then
   for f in $XDG_CONFIG_HOME/profile.d/*; do
      . "$f"
   done
fi


#--[ dirstack ]--
#- Handle DIRSTACK to have a buffer of last visited directories
DIRSTACKFILE="$HOME/.cache/zsh/dirs"
if [[ -f $DIRSTACKFILE ]] && [[ $#dirstack -eq 0 ]]; then
   dirstack=( ${(f)"$(< $DIRSTACKFILE)"} )
   #[[ -d $dirstack[1] ]] && cd $dirstack[1]
fi
chpwd() {
   print -l $PWD ${(u)dirstack} >$DIRSTACKFILE
}
DIRSTACKSIZE=50

setopt autopushd pushdsilent pushdtohome pushdignoredups


#--[ Completion System ]--
# add ~/.config/zsh/completion to completion paths
# NOTE: this needs to be a directory with 0755 permissions, 
# otherwise you will get "insecure" warnings on shell load!
fpath=("$XDG_CONFIG_HOME/zsh/completion" $fpath)

# set autocomplete for aliases
setopt completealiases

# Load
autoload -Uz compinit
compinit
# Definitions
zstyle ':completion:*:(ssh|scp|sftp|rsync):*' hosts "${(z@)${${(f@)$(<${HOME}/.ssh/known_hosts)}%%\ *}%%,*}"
zstyle ":completion:*" auto-description "specify: %d"
zstyle ":completion:*" completer _expand _complete _correct _approximate
zstyle ":completion:*" format "Completing %d"
zstyle ":completion:*" group-name ""
zstyle ":completion:*" menu select=2
zstyle ":completion:*:default" list-colors ${(s.:.)LS_COLORS}
zstyle ":completion:*" list-colors ""
zstyle ":completion:*" list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ":completion:*" matcher-list "" "m:{a-z}={A-Z}" "m:{a-zA-Z}={A-Za-z}" "r:|[._-]=* r:|=* l:|=*"
zstyle ":completion:*" menu select=long
zstyle ":completion:*" select-prompt %SScrolling active: current selection at %p%s
zstyle ":completion:*" verbose true
# > `kill <tab>` will open fzf with processes to kill.
zstyle ":completion:*:*:kill:*:processes" list-colors "=(#b) #([0-9]#)*=0=01;31"
zstyle ":completion:*:kill:*" command "ps -u $USER -o pid,%cpu,tty,cputime,cmd"
#- complete 'cd -<tab>' with menu
zstyle ':completion:*:*:cd:*:directory-stack' menu yes select


#--[ Bindings ]--
#- Run cdlast, which shows last opened directories in fzf an cds to selection
zle -N cdlast
bindkey '^L' cdlast
#- Set Vi key mode
bindkey -v
bindkey '^P' up-history
bindkey '^N' down-history
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char
bindkey '^w' backward-kill-word
# bindkey '^r' history-incremental-search-backward # let fzf handle this
bindkey '^u' backward-kill-line
bindkey '^k' kill-line
bindkey '^[[3~' delete-char
# bind editing command in vim to ctrl-e
autoload edit-command-line
zle -N edit-command-line
bindkey "^e" edit-command-line

#--[ tty config ]--
if [ "$TERM" = "linux" ]; then
    echo -en "\e]P0191a1d" #black
    echo -en "\e]P1f3b2ab" #red
    echo -en "\e]P2bad260" #green
    echo -en "\e]P3eac58d" #yellow
    echo -en "\e]P4c6e5f8" #blue
    echo -en "\e]P5e4c6ed" #magenta
    echo -en "\e]P6c6f3e6" #cyan
    echo -en "\e]P7ece3d0" #white
    #echo -en "\e]P7333841" #darkgrey

    echo -en "\e]P85a6373" #lightgrey
    echo -en "\e]P9ea4439" #darkred
    echo -en "\e]PA50763d" #darkgreen
    echo -en "\e]PBfab81d" #brown
    echo -en "\e]PC446fa6" #darkblue
    echo -en "\e]PD84678f" #darkmagenta
    echo -en "\e]PE4f7b6c" #darkcyan

    echo -en "\e]PFece3d0" #white
    clear #for background artifacting
fi


#--[ Window Title ]--

case $TERM in
termite|*xterm*|rxvt|rxvt-unicode|rxvt-256color|rxvt-unicode-256color|(dt|k|E)term|st*)
    precmd () {
      vcs_info
      print -Pn "\e]0;[%n@%M][%~]%#\a"
    } 
    preexec () { print -Pn "\e]0;[%n@%M][%~]%# ($1)\a" }
    ;;
  screen|screen-256color)
    precmd () { 
      vcs_info
      print -Pn "\e]83;title \"$1\"\a" 
      print -Pn "\e]0;$TERM - (%L) [%n@%M]%# [%~]\a" 
    }
    preexec () { 
      print -Pn "\e]83;title \"$1\"\a" 
      print -Pn "\e]0;$TERM - (%L) [%n@%M]%# [%~] ($1)\a" 
    }
    ;; 
esac


compdef "_files -W $HOME/syncthing/smalldocs-encrypted/quicknotes/ -/" n
compdef "_files -W $HOME/syncthing/smalldocs-encrypted/quicknotes/ -/" nrm
compdef "_files -W $HOME/syncthing/smalldocs-encrypted/quicknotes/ -/" nmv


#EOF

# pnpm
export PNPM_HOME="/home/martinfraga/.local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end
