
if [[ -z "$XDG_CONFIG_HOME" ]]; then
   export XDG_CONFIG_HOME="$HOME/.config"
fi
if [[ -z "$XDG_CACHE_HOME" ]]; then
   export XDG_CACHE_HOME="$HOME/.config"
fi
if [[ -z "$XDG_DATA_HOME" ]]; then
   export XDG_DATA_HOME="$HOME/.local/share"
fi
if [[ -z "$XDG_DESKTOP_DIR" ]]; then
   export XDG_DESKTOP_DIR="$HOME/Desktop"
fi
if [[ -z "$XDG_DOCUMENTS_DIR" ]]; then
   export XDG_DOCUMENTS_DIR="$HOME/Documents"
fi
if [[ -z "$XDG_DOWNLOAD_DIR" ]]; then
   export XDG_DOWNLOAD_DIR="$HOME/Downloads"
fi
if [[ -z "$XDG_MUSIC_DIR" ]]; then
   export XDG_MUSIC_DIR="$HOME/Music"
fi
if [[ -z "$XDG_PICTURES_DIR" ]]; then
   export XDG_PICTURES_DIR="$HOME/Pictures"
fi
if [[ -z "$XDG_PUBLICSHARE_DIR" ]]; then
   export XDG_PUBLICSHARE_DIR="$HOME/Public"
fi
if [[ -z "$XDG_TEMPLATES_DIR" ]]; then
   export XDG_TEMPLATES_DIR="$HOME/Templates"
fi
if [[ -z "$XDG_VIDEOS_DIR" ]]; then
   export XDG_VIDEOS_DIR="$HOME/Videos"
fi




# PATH
if [[ -d "$HOME/gitland/scripts" ]]; then
   export PATH="$HOME/gitland/scripts:$PATH"
fi

# PATH
if [[ -d "$HOME/gitland/scripts" ]]; then
   export PATH="$HOME/gitland/scripts:$PATH"
fi

if [[ -d "/usr/libexec/icecc/bin" ]]; then
  export PATH="/usr/libexec/icecc/bin:$PATH"
fi

if [[ -d "/usr/lib64/openmpi/bin" ]]; then
  export PATH="/usr/lib64/openmpi/bin:$PATH"
fi

if [[ -d "/usr/lib64/openmpi/lib" ]]; then
  export PATH="/usr/lib64/openmpi/lib:$PATH"
fi

if [[ -d "$HOME/bin" ]]; then
   export PATH="$HOME/bin:$PATH"
fi

export VISUAL=nvim
export EDITOR=nvim
export BROWSER=firefox
export QT_QPA_PLATFORMTHEME="qt5ct"

export XMODIFIERS='@im=SCIM'
export GTK_IM_MODULE=xim
export QT_IM_MODULE=xim

keychain --clear --quiet

