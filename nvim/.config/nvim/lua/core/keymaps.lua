-- NOTE: leader keys are set in settings.lua


local keymap_set = function(lkeys, lfunc, ldesc)
  vim.keymap.set('n', lkeys, lfunc, { desc = ldesc })
end

keymap_set('<leader>bn', '<esc>:bn<CR>', '[B]uffer: [N]ext')
keymap_set('<leader>bp', '<esc>:bp<CR>', '[B]uffer: [P]revious')
keymap_set('<leader>bd', '<esc>:bd<CR>', '[B]uffer: [D]elete')
-- TODO keymap_set('<leader>bD', '<esc>:bD<CR>', '[B]uffer: [D]elete buffer and window')
keymap_set('<leader>bl', '<esc>:bl<CR>', '[B]uffer: [L]ist')

keymap_set('gl', vim.diagnostic.open_float, 'Diag: Open Float')
keymap_set('gtp', vim.diagnostic.goto_prev, 'Diag: GoTo Previous')
keymap_set('gtn', vim.diagnostic.goto_next, 'Diag: GoTo Next')

