require("neorg").setup({
  load = {
    ["core.defaults"] = {},
    ["core.dirman"] = {
      config = {
        workspaces = {
          notes = "/home/martinfraga/norg_notes",
          test = "/home/martinfraga/norg_test",
        },
      },
    },
    ["core.concealer"] = {
      config = { -- We added a `config` table!
        icon_preset = "basic", -- And we set our option here.
      },
    },
    --["core.completion"] = {},
    ["core.itero"] = {},
  },
})


-- Keybindings!
