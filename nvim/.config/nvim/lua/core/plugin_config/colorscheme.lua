require("gruvbox").setup({})
require("kanagawa").setup({
  theme = "wave",              -- Load "wave" theme when 'background' option is not set
  background = {               -- map the value of 'background' option to a theme
    dark = "wave",           -- try "dragon" !
    light = "lotus"
  },
})

vim.o.termguicolors = true
vim.o.background = "dark"
vim.cmd[[colorscheme kanagawa]]
